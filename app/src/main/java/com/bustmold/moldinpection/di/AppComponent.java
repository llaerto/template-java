package com.bustmold.moldinpection.di;

import com.bustmold.moldinpection.common.AppModule;
import com.bustmold.moldinpection.network.NetModule;

import javax.inject.Singleton;

import dagger.Component;


@Singleton
@Component(modules = {AppModule.class, NetModule.class})
public interface AppComponent {
    //    void inject(StartActivity activity);
}
