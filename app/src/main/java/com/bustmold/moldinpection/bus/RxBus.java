package com.bustmold.moldinpection.bus;


import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.subjects.PublishSubject;

public class RxBus {

    public RxBus() {

    }

    private final PublishSubject<Object> bus = PublishSubject.create();

    public void send(Object o) {
        bus.onNext(o);
    }

    @SuppressWarnings("unchecked")
    public <T> Disposable register(final Class<T> eventClass, Consumer<T> onNext) {
        return bus
                .filter(event -> event.getClass().equals(eventClass))
                .map(obj -> (T) obj)
                .subscribe(onNext);
    }
}
