package com.bustmold.moldinpection.presenter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.bustmold.moldinpection.common.AppError;
import com.bustmold.moldinpection.di.AppComponent;
import com.bustmold.moldinpection.interfaces.model.BaseModelActions;
import com.bustmold.moldinpection.interfaces.presenter.BasePresenterActions;
import com.bustmold.moldinpection.interfaces.view.BaseViewActions;

import java.lang.ref.WeakReference;

import io.reactivex.disposables.CompositeDisposable;

public abstract class BasePresenter<T extends BaseViewActions, M extends BaseModelActions>
        implements BasePresenterActions.ViewActions, BasePresenterActions.ModelActions {

    private WeakReference<T> view;
    private M modelController;
    private boolean isRunning;
    private CompositeDisposable compositeSubscription;

    @SuppressWarnings("all")
    protected BasePresenter(T view) {
        isRunning = true;
        this.view = new WeakReference<>(view);
        modelController = createModelInstance();
        compositeSubscription = new CompositeDisposable();
        subscribeOnEvents();
    }

    @Override
    public void setView(BaseViewActions view) {
        this.view = new WeakReference<>((T) view);
        isRunning = true;
        modelController = createModelInstance();
        compositeSubscription = new CompositeDisposable();
        subscribeOnEvents();
    }

    /* Inner methods */

    protected abstract M createModelInstance();

    protected T getView() {
        if (isRunning && view != null) {
            return view.get();
        } else {
            return null;
        }
    }

    protected M getModel() {
        return modelController;
    }

    protected CompositeDisposable getCompositeSubscription() {
        return compositeSubscription;
    }

    protected Context getContext() {
        if (view != null) {
            return view.get().getContext();
        } else {
            return null;
        }
    }

    @SuppressWarnings("all")
    protected void subscribeOnEvents() {
        /* here should be subscription to events*/
    }

    /* View calls */

    public void onViewDestroyed() {
        compositeSubscription.clear();
    }

    public void onDestroy() {
        view = null;
        isRunning = false;
        modelController.onDestroy();
    }

    @Override
    public AppComponent injector() {
        return getView() != null ? getView().injector() : null;
    }

    /* Model callbacks */

    public void onError(@Nullable AppError error) {
        if (getView() != null) {
            getView().unlockUi();
        }
        if (error != null && getView() != null) {
            getView().showError(error);
        }
    }

    /* Lifecycle methods */

    @Override
    public void onResume() {
        //override if necessary.
    }

    @Override
    public void onPause() {
        //override if necessary.
    }

    @Override
    public void onStart() {
        //override if necessary.
    }

    @Override
    public void onStop() {
        //override if necessary.
    }

    @Override
    public void onActivityResult(int request, int result, Intent data) {
        //override if necessary.
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //override if necessary.
    }
}
