package com.bustmold.moldinpection.presenter;

import com.bustmold.moldinpection.R;
import com.bustmold.moldinpection.common.AppError;
import com.bustmold.moldinpection.entity.InfoEntity;
import com.bustmold.moldinpection.interfaces.model.PagerModelActions;
import com.bustmold.moldinpection.interfaces.presenter.PagerPresenterActions;
import com.bustmold.moldinpection.interfaces.view.PagerViewActions;
import com.bustmold.moldinpection.model.PagerModel;

import java.util.List;

/**
 * Created by Mike Diachenko
 * on 08-Jun-17.
 * from Alienware
 */

public class PagerPresenter extends BasePresenter<PagerViewActions, PagerModelActions>
        implements PagerPresenterActions.ViewActions, PagerPresenterActions.ModelActions {

    public PagerPresenter(PagerViewActions view) {
        super(view);
    }

    @Override
    public void onViewCreated() {

    }

    @Override
    protected PagerModelActions createModelInstance() {
        return new PagerModel(this);
    }

    @Override
    public void onCallClicked() {
        if (getView() != null && getModel() != null) {
            getView().callUs(getModel().getPhone());
        }
    }

    @Override
    public void onSendClicked(InfoEntity infoEntity, List<String> photos) {
        if (!infoEntity.isFilled()) {
            AppError error = new AppError();
            error.setMessage(getContext().getString(R.string.validation_failed_message));
            getView().showError(error);
            return;
        }
        if (photos == null || photos.size() == 0) {
            AppError error = new AppError();
            error.setMessage(getContext().getString(R.string.validation_failed_photos));
            getView().showError(error);
            return;
        }
        if (getView() != null && getModel() != null) {
            getView().showProgressBar();
            getModel().send(infoEntity, photos);
        }
    }

    @Override
    public void onSuccessSend() {
        if (getView() != null) {
            getView().unlockUi();
        }
    }
}
