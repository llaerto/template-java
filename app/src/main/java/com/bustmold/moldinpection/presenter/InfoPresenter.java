package com.bustmold.moldinpection.presenter;

import com.bustmold.moldinpection.entity.InfoEntity;
import com.bustmold.moldinpection.interfaces.model.InfoModelActions;
import com.bustmold.moldinpection.interfaces.presenter.InfoPresenterActions;
import com.bustmold.moldinpection.interfaces.view.InfoViewActions;
import com.bustmold.moldinpection.model.InfoModel;

/**
 * Created by Mike Diachenko
 * on 08-Jun-17.
 * from Alienware
 */

public class InfoPresenter extends BasePresenter<InfoViewActions, InfoModelActions>
        implements InfoPresenterActions.ViewActions, InfoPresenterActions.ModelActions {

    public InfoPresenter(InfoViewActions view) {
        super(view);
    }

    @Override
    public void onViewCreated() {
        if (getModel() != null) {
            getModel().preloadData();
        }
    }

    @Override
    protected InfoModelActions createModelInstance() {
        return new InfoModel(this);
    }

    @Override
    public void onDataPreloaded(InfoEntity entity) {
        if (getView() != null) {
            getView().setDataPreloaded(entity);
        }
    }
}
