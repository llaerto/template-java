package com.bustmold.moldinpection.presenter;

import com.bustmold.moldinpection.interfaces.model.SplashModelActions;
import com.bustmold.moldinpection.interfaces.presenter.SplashPresenterActions;
import com.bustmold.moldinpection.interfaces.view.SplashViewActions;
import com.bustmold.moldinpection.model.SplashModel;

/**
 * Created by Mike Diachenko
 * on 09-Jun-17.
 * from Alienware
 */

public class SplashPresenter extends BasePresenter<SplashViewActions, SplashModelActions>
        implements SplashPresenterActions.ViewActions, SplashPresenterActions.ModelActions {

    public SplashPresenter(SplashViewActions view) {
        super(view);
    }

    @Override
    public void onViewCreated() {
        if (getModel() != null) {
            getModel().startSplash();
        }
    }

    @Override
    protected SplashModelActions createModelInstance() {
        return new SplashModel(this);
    }

    @Override
    public void onProgressUpdate(int progress) {
        if (getView() != null) {
            getView().updateProgress(progress);
        }
    }

    @Override
    public void onSplashFinished() {
        if (getView() != null) {
            getView().launchApp();
        }
    }
}
