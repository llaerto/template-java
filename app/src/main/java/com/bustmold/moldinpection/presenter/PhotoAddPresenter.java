package com.bustmold.moldinpection.presenter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;

import com.bustmold.moldinpection.R;
import com.bustmold.moldinpection.interfaces.model.PhotoAddModelActions;
import com.bustmold.moldinpection.interfaces.presenter.PhotoAddPresenterActions;
import com.bustmold.moldinpection.interfaces.view.PhotoAddViewActions;
import com.bustmold.moldinpection.model.PhotoAddModel;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mike Diachenko
 * on 08-Jun-17.
 * from Alienware
 */

public class PhotoAddPresenter extends BasePresenter<PhotoAddViewActions, PhotoAddModelActions>
        implements PhotoAddPresenterActions.ViewActions, PhotoAddPresenterActions.ModelActions {
    private List<String> photos;
    private static final int REQUEST_CAMERA_PERMISSION = 1001;
    private static final int REQUEST_GALLERY_PERMISSION = 1002;
    private static final int REQUEST_GALLERY_PERMISSION_CAMERA = 1003;
    private static final int CAMERA_INTENT = 101;
    private static final int GALLERY_INTENT = 102;
    private Uri tempUri;
    private String absolute;

    public PhotoAddPresenter(PhotoAddViewActions view) {
        super(view);
        photos = new ArrayList<>();
    }

    @Override
    public void onViewCreated() {
        if (getView() != null) {
            getView().setupAdapter(photos);
        }
    }

    @Override
    protected PhotoAddModelActions createModelInstance() {
        return new PhotoAddModel(this);
    }

    @Override
    public void onAddClick() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.choose_source_title).setMessage(R.string.choose_source_message);
        builder.setPositiveButton(R.string.camera, (dialogInterface, i) -> fetchFromCamera(dialogInterface));
        builder.setNeutralButton(R.string.gallery, (dialogInterface, i) -> fetchFromGallery(dialogInterface));
        builder.setNegativeButton(R.string.cancel, ((dialog, which) -> dialog.dismiss()));
        builder.create().show();
    }

    @Override
    public void onActivityResult(int request, int result, Intent data) {
        if ((request == CAMERA_INTENT || request == GALLERY_INTENT) && result == Activity.RESULT_OK) {
            addPhoto(data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_GALLERY_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            getView().askGallery(GALLERY_INTENT, tempUri);
        }
        if (requestCode == REQUEST_CAMERA_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            prepareFileForIntent();
            getView().askCamera(CAMERA_INTENT, tempUri);
        }
        if (requestCode == REQUEST_GALLERY_PERMISSION_CAMERA && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            prepareFileForIntent();
            getView().askCamera(CAMERA_INTENT, tempUri);
        }
    }

    @Override
    public List<String> getPhotos() {
        return photos;
    }

    private void fetchFromGallery(DialogInterface dialogInterface) {
        dialogInterface.dismiss();
        if (getView() != null) {
            if (!getView().isGalleryPermitted()) {
                getView().requestGallery(REQUEST_GALLERY_PERMISSION);
                return;
            }
            getView().askGallery(GALLERY_INTENT, tempUri);
        }
    }

    private void fetchFromCamera(DialogInterface dialogInterface) {
        dialogInterface.dismiss();
        if (getView() != null) {
            if (!getView().isGalleryPermitted()) {
                getView().requestGallery(REQUEST_GALLERY_PERMISSION_CAMERA);
                return;
            }
            if (!getView().isCameraPermitted()) {
                getView().requestCamera(REQUEST_CAMERA_PERMISSION);
                return;
            }
            prepareFileForIntent();
            getView().askCamera(CAMERA_INTENT, tempUri);
        }
    }

    private void addPhoto(Intent data) {
        if (data != null && data.getData() != null) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            assert selectedImage != null;
            Cursor cursor = getContext().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            assert cursor != null;
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();
            photos.add(picturePath);
            if (getView() != null) {
                getView().addPhoto();
            }
        } else if (absolute != null) {
            photos.add(absolute);
            if (getView() != null) {
                getView().addPhoto();
            }
            absolute = null;
        }
    }
    private void prepareFileForIntent() {
        File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = null;
        try {
            image = File.createTempFile(
                    System.currentTimeMillis() + "",  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );
            absolute = image.getAbsolutePath();
        } catch (IOException e) {
            e.printStackTrace();
        }
        tempUri = FileProvider.getUriForFile(getContext(),
                "com.bustmold.moldinpection.fileprovider",
                image);
    }
}
