package com.bustmold.moldinpection.network.api;

import retrofit2.Retrofit;

abstract class BaseApiManager<T> {

    T apiInterface;

    BaseApiManager(Retrofit retrofit, Class<T> clazz){
        apiInterface = retrofit.create(clazz);
    }

}
