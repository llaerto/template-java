package com.bustmold.moldinpection.network.api;

import com.bustmold.moldinpection.network.api.retrofit.ApiInterface;

import retrofit2.Retrofit;


public class ExampleApiManager extends BaseApiManager<ApiInterface> {
    //rename this class according to ApiInterface.
    public static ExampleApiManager newInstance(Retrofit retrofit) {
        return new ExampleApiManager(retrofit);
    }

    private ExampleApiManager(Retrofit retrofit) {
        super(retrofit, ApiInterface.class);
    }

    //below should be api calls
}
