package com.bustmold.moldinpection.view.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.bustmold.moldinpection.R;
import com.bustmold.moldinpection.view.fragments.PhotoAddFragment;
import com.bustmold.moldinpection.view.fragments.InfoFragment;

import java.lang.ref.WeakReference;

/**
 * Created by Mike Diachenko
 * on 08-Jun-17.
 * from Alienware
 */

public class PagerAdapter extends FragmentStatePagerAdapter {
    private WeakReference<Context> contextWeakReference;

    private InfoFragment first;
    private PhotoAddFragment second;

    public PagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.contextWeakReference = new WeakReference<>(context);
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            if (first == null) {
                first = new InfoFragment();
            }
            return first;
        } else {
            if (second == null) {
                second = new PhotoAddFragment();
            }
            return second;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0) {
            return contextWeakReference.get().getString(R.string.tab_info);
        } else {
            return contextWeakReference.get().getString(R.string.tab_add);
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    public void onActivityResult(int request, int result, Intent data) {
        if (first != null) {
            first.onActivityResult(request, result, data);
        }
        if (second != null) {
            second.onActivityResult(request, result, data);
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (first != null) {
            first.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
        if (second != null) {
            second.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public InfoFragment getFirst() {
        return first;
    }

    public void setFirst(InfoFragment first) {
        this.first = first;
    }

    public PhotoAddFragment getSecond() {
        return second;
    }

    public void setSecond(PhotoAddFragment second) {
        this.second = second;
    }
}
