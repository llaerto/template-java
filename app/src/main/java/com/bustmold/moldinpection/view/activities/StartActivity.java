package com.bustmold.moldinpection.view.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.bustmold.moldinpection.R;

public class StartActivity extends BaseActivity {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        injector().inject(this);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.start_activity;
    }

    @Override
    protected void initView(Bundle savedInstanceState) {

    }
}
