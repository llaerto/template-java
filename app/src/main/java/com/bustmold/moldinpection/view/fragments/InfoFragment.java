package com.bustmold.moldinpection.view.fragments;

import android.content.Context;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.bustmold.moldinpection.R;
import com.bustmold.moldinpection.entity.InfoEntity;
import com.bustmold.moldinpection.interfaces.presenter.InfoPresenterActions;
import com.bustmold.moldinpection.interfaces.view.InfoViewActions;
import com.bustmold.moldinpection.presenter.InfoPresenter;

import butterknife.BindView;

/**
 * Created by Mike Diachenko
 * on 08-Jun-17.
 * from Alienware
 */

public class InfoFragment extends BaseFragment<InfoPresenterActions.ViewActions> implements InfoViewActions {
    public static final String TAG = InfoFragment.class.getSimpleName();

    @BindView(R.id.name) EditText name;
    @BindView(R.id.email) EditText mail;
    @BindView(R.id.phone) EditText phone;
    @BindView(R.id.address) EditText country;
    @BindView(R.id.description) EditText description;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_info;
    }

    @Override
    protected InfoPresenterActions.ViewActions createPresenter() {
        return new InfoPresenter(this);
    }

    @Override
    public String getTagName() {
        return TAG;
    }

    @Override
    public void setDataPreloaded(InfoEntity entity) {
        name.setText(entity.getName());
        mail.setText(entity.getEmail());
        phone.setText(entity.getPhone());
        country.setText(entity.getCountry());
        description.setText(entity.getDescription());
    }

    @Override
    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(name.getWindowToken(), 0);
    }

    public InfoEntity getFilledData() {
        InfoEntity entity = new InfoEntity();
        entity.setName(name.getText().toString());
        entity.setEmail(mail.getText().toString());
        entity.setPhone(phone.getText().toString());
        entity.setCountry(country.getText().toString());
        entity.setDescription(description.getText().toString());
        return entity;
    }
}
