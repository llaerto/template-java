package com.bustmold.moldinpection.view.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ProgressBar;

import com.bustmold.moldinpection.R;
import com.bustmold.moldinpection.interfaces.presenter.SplashPresenterActions;
import com.bustmold.moldinpection.interfaces.view.SplashViewActions;
import com.bustmold.moldinpection.presenter.SplashPresenter;

import butterknife.BindView;

/**
 * Created by Mike Diachenko
 * on 09-Jun-17.
 * from Alienware
 */

public class SplashFragment extends BaseFragment<SplashPresenterActions.ViewActions> implements SplashViewActions {
    public static final String TAG = SplashFragment.class.getSimpleName();

    public interface FragmentEventListener {
        void launchSplash();
    }

    private FragmentEventListener fragmentEventListener;

    @BindView(R.id.progressBar) ProgressBar progressBar;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_splash;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getPresenter() != null) {
            getPresenter().onViewCreated();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentEventListener) {
            fragmentEventListener = (FragmentEventListener) context;
        }
    }

    @Override
    public void onDetach() {
        fragmentEventListener = null;
        super.onDetach();
    }

    @Override
    protected SplashPresenterActions.ViewActions createPresenter() {
        return new SplashPresenter(this);
    }

    @Override
    public String getTagName() {
        return TAG;
    }

    @Override
    public void updateProgress(int progress) {
        progressBar.setProgress(progress);
    }

    @Override
    public void launchApp() {
        if (fragmentEventListener != null) {
            fragmentEventListener.launchSplash();
        }
    }
}
