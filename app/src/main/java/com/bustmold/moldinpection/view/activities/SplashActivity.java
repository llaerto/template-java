package com.bustmold.moldinpection.view.activities;

import android.os.Bundle;

import com.bustmold.moldinpection.R;
import com.bustmold.moldinpection.view.fragments.SplashFragment;

public class SplashActivity extends BaseActivity implements SplashFragment.FragmentEventListener {

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_single_fragment;
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setLogo(R.drawable.icon);
        }
        SplashFragment fragment = (SplashFragment) getSupportFragmentManager().findFragmentByTag(SplashFragment.TAG);
        if (fragment == null) {
            fragment = new SplashFragment();
        }
        openFragment(fragment, false);
    }


    @Override
    public void launchSplash() {
        launchActivity(PagerViewActivity.class, true);
    }
}
