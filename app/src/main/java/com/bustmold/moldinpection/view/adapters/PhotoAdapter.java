package com.bustmold.moldinpection.view.adapters;

import android.net.Uri;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.bustmold.moldinpection.R;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mike Diachenko
 * on 08-Jun-17.
 * from Alienware
 */

public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.ViewHolder> {
    private List<String> photos;

    public interface RemoveListener {
        void onItemRemove(String photo);
    }

    private RemoveListener removeListener;

    public PhotoAdapter(List<String> photos) {
        this.photos = photos;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image, null);
        ViewHolder holder = new ViewHolder(view);
        holder.removeButton.setOnClickListener(v -> processRemoveClick(holder));
        return holder;
    }

    private void processRemoveClick(ViewHolder holder) {
        photos.remove(holder.getAdapterPosition());
        notifyItemRemoved(holder.getAdapterPosition());
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        int width = (holder.image.getContext().getResources().getDisplayMetrics().widthPixels -
                holder.image.getResources().getDimensionPixelSize(R.dimen.margin) * 2) / 2;
        CardView.LayoutParams params = new CardView.LayoutParams(width, width);
        holder.image.setLayoutParams(params);
        Glide.with(holder.image.getContext())
//                .load(FileProvider.getUriForFile(holder.image.getContext(),
//                        "com.bustmold.moldinpection.fileprovider",
//                        new File(photos.get(position))))
                .load(photos.get(position))
                .placeholder(R.drawable.icons_camera)
                .error(R.drawable.icons_camera)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        Log.w("TEST", e);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        return false;
                    }
                })
                .into(holder.image);
    }

    @Override
    public int getItemCount() {
        return photos != null ? photos.size() : 0;
    }

    public void setRemoveListener(RemoveListener removeListener) {
        this.removeListener = removeListener;
    }

    public List<String> getPhotos() {
        return photos;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image) ImageView image;
        @BindView(R.id.removeButton) ImageView removeButton;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
