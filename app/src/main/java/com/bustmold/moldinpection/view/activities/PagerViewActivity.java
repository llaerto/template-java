package com.bustmold.moldinpection.view.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.content.PermissionChecker;
import android.support.v4.view.ViewPager;
import android.widget.Button;

import com.bustmold.moldinpection.R;
import com.bustmold.moldinpection.common.AppError;
import com.bustmold.moldinpection.interfaces.presenter.PagerPresenterActions;
import com.bustmold.moldinpection.interfaces.view.PagerViewActions;
import com.bustmold.moldinpection.presenter.PagerPresenter;
import com.bustmold.moldinpection.view.adapters.PagerAdapter;
import com.bustmold.moldinpection.view.fragments.PhotoAddFragment;

import java.util.List;

import butterknife.BindView;

/**
 * Created by Mike Diachenko
 * on 08-Jun-17.
 * from Alienware
 */

public class PagerViewActivity extends BaseViewActivity<PagerPresenterActions.ViewActions>
        implements PagerViewActions, PhotoAddFragment.FragmentEventListener {
    public static final String TAG = PagerViewActivity.class.getSimpleName();

    @BindView(R.id.tablayout) TabLayout tabLayout;
    @BindView(R.id.viewpager) ViewPager viewPager;
    @BindView(R.id.buttonSend) Button sendButton;
    @BindView(R.id.buttonCall) Button callButton;
    private PagerAdapter adapter;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_main;
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setLogo(R.drawable.icon);
        }
        adapter = new PagerAdapter(getSupportFragmentManager(), this);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        sendButton.setOnClickListener(v -> processSendClick());
        callButton.setOnClickListener(v -> processCallClick());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        adapter.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        adapter.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void processSendClick() {
        if (getPresenter() != null) {
            getPresenter().onSendClicked(adapter.getFirst().getFilledData());
        }
    }

    private void processCallClick() {
        if (getPresenter() != null) {
            getPresenter().onCallClicked();
        }
    }

    @Override
    protected PagerPresenterActions.ViewActions createPresenter() {
        return new PagerPresenter(this);
    }

    @Override
    public String getTagName() {
        return TAG;
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void callUs(String phone) {
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone));
        startActivity(intent);
    }

    @Override
    public void showError(@Nullable AppError error) {
        if (error != null && error.getMessage() != null && !error.getMessage().isEmpty()) {
            shortSnack(error.getMessage());
        }
    }

    @Override
    public boolean isCameraPermitted() {
        int test = PermissionChecker.checkSelfPermission(this,
                Manifest.permission.CAMERA);
        return test == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public boolean isGalleryPermitted() {
        int test = PermissionChecker.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return test == PackageManager.PERMISSION_GRANTED;
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void requestCameraPermission(int request) {
        if (!isCameraPermitted()) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, request);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void requestGalleryPermission(int request) {
        if (!isGalleryPermitted()) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, request);
        }
    }

    @Override
    public void launchCameraIntent(int request, Uri tempUri) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, tempUri);
        startActivityForResult(intent, request);
    }

    @Override
    public void launchGalleryIntent(int request, Uri tempUri) {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, tempUri);
        startActivityForResult(intent, request);
    }

}
