package com.bustmold.moldinpection.view.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;

import com.bustmold.moldinpection.App;
import com.bustmold.moldinpection.R;
import com.bustmold.moldinpection.common.StateMaintainer;
import com.bustmold.moldinpection.di.AppComponent;
import com.bustmold.moldinpection.interfaces.presenter.BasePresenterActions;
import com.bustmold.moldinpection.interfaces.view.BaseViewActions;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public abstract class BaseViewActivity<T extends BasePresenterActions.ViewActions> extends BaseActivity implements BaseViewActions {
    @Nullable
    @BindView(R.id.progress_view)
    View progressView;
    @Nullable
    @BindView(R.id.empty_view)
    View emptyView;

    private T presenter;
    private StateMaintainer stateMaintainer;
    private Unbinder unbinder;
    private Animation fadeInAnimation, fadeOutAnimation;
    protected ProgressDialog progressDialog;
    protected boolean disabled;

     /* Superclass methods */

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fadeOutAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.fast_fade_out);
        fadeInAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.fast_fade_in);
        restore();
        unbinder = ButterKnife.bind(this);
    }

    protected void restore() {
        stateMaintainer = StateMaintainer.getInstance();
        if (stateMaintainer.firstTimeInForActivity(getTagName())) {
            initialize();
        } else {
            reinitialize();
        }
    }

    protected void resetView() {
        if (presenter != null) {
            presenter.setView(this);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            hideKeyboard();
        } catch (Exception e) {
            //nothing to do
        }
        if (presenter != null) {
            presenter.onPause();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (presenter != null) {
            presenter.onStart();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (presenter != null) {
            presenter.onStop();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (presenter != null) {
            presenter.onResume();
        }
    }

    @Override
    public void onDestroy() {
        stateMaintainer.delete(getTagName());
        unbinder.unbind();
        presenter.onDestroy();
        presenter = null;
        super.onDestroy();
    }

    public AppComponent injector() {
        return ((App) getApplication()).getAppComponent();
    }

    protected T getPresenter() {
        return presenter;
    }

    @Override
    public void showProgressBar() {
        if (progressDialog != null) {
            progressDialog.show();
        }
    }

    @Override
    public void showProgressView() {
        if (progressView != null) {
            progressView.setVisibility(View.VISIBLE);
        }
        if (emptyView != null) {
            emptyView.setVisibility(View.GONE);
        }
    }

    @Override
    public void showEmptyView() {
        if (emptyView != null) {
            emptyView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void lockUi() {
        disabled = true;
    }

    @Override
    public boolean isUiLocked() {
        return disabled;
    }

    @Override
    public void unlockUi() {
        disabled = false;
        hideProgress();
    }

    @Override
    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        presenter.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        presenter.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    /* Inner methods */

    protected void fadeAction(View view, boolean in) {
        view.setVisibility(in ? View.VISIBLE : View.GONE);
        view.startAnimation(in ? fadeInAnimation : fadeOutAnimation);
    }

    private void initialize() {
        presenter = createPresenter();
        stateMaintainer.put(getTagName(), presenter);
        initProgressDialog();
    }

    private void initProgressDialog() {
        if (getProgressTitle() == null && getProgressMessage() == null) {
            return;
        }
        progressDialog = new ProgressDialog(getContext());
        if (getProgressTitle() != null) {
            progressDialog.setTitle(getProgressTitle());
        }
        if (getProgressMessage() != null) {
            progressDialog.setMessage(getProgressMessage());
        }
    }

    private void hideProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
        if (progressView != null) {
            progressView.setVisibility(View.GONE);
        }
    }

    private void reinitialize() {
        presenter = stateMaintainer.get(getTagName());
        if (presenter == null) {
            initialize();
        } else {
            resetView();
        }
    }

    protected String getProgressTitle() {
        return null;
    }

    protected String getProgressMessage() {
        return getString(R.string.progress_message);
    }

    /* Abstract methods */

    protected abstract T createPresenter();

    public abstract String getTagName();
}
