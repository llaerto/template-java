package com.bustmold.moldinpection.view.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;

import com.bustmold.moldinpection.R;
import com.bustmold.moldinpection.interfaces.presenter.PhotoAddPresenterActions;
import com.bustmold.moldinpection.interfaces.view.PhotoAddViewActions;
import com.bustmold.moldinpection.presenter.PhotoAddPresenter;
import com.bustmold.moldinpection.view.adapters.PhotoAdapter;
import com.bustmold.moldinpection.view.custom.SpacesItemDecoration;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by Mike Diachenko
 * on 08-Jun-17.
 * from Alienware
 */

public class PhotoAddFragment extends BaseFragment<PhotoAddPresenterActions.ViewActions>
        implements PhotoAddViewActions {
    public static final String TAG = PhotoAddFragment.class.getSimpleName();

    public interface FragmentEventListener {
        boolean isCameraPermitted();

        boolean isGalleryPermitted();

        void requestCameraPermission(int request);

        void requestGalleryPermission(int request);

        void launchCameraIntent(int request, Uri tempUri);

        void launchGalleryIntent(int request, Uri tempUri);
    }

    private FragmentEventListener fragmentEventListener;

    @BindView(R.id.addButton) ImageButton addButton;
    @BindView(R.id.recycler_view) RecyclerView recyclerView;


    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_add;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentEventListener) {
            fragmentEventListener = (FragmentEventListener) context;
        }
    }

    @Override
    public void onDetach() {
        fragmentEventListener = null;
        super.onDetach();
    }

    @Override
    protected PhotoAddPresenterActions.ViewActions createPresenter() {
        return new PhotoAddPresenter(this);
    }

    @Override
    public String getTagName() {
        return TAG;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        addButton.setOnClickListener(v -> processAddClick());
        if (getPresenter() != null) {
            getPresenter().onViewCreated();
        }
    }

    @Override
    public void setupAdapter(List<String> photos) {
        recyclerView.setAdapter(new PhotoAdapter(photos));
        recyclerView.addItemDecoration(new SpacesItemDecoration(getResources().getDimensionPixelSize(R.dimen.margin_s)));
    }

    @Override
    public void addPhoto() {
        recyclerView.getAdapter().notifyItemInserted(recyclerView.getAdapter().getItemCount() - 1);
    }

    @Override
    public boolean isCameraPermitted() {
        return fragmentEventListener != null && fragmentEventListener.isCameraPermitted();
    }

    @Override
    public boolean isGalleryPermitted() {
        return fragmentEventListener != null && fragmentEventListener.isGalleryPermitted();
    }

    @Override
    public void requestCamera(int request) {
        if (fragmentEventListener != null) {
            fragmentEventListener.requestCameraPermission(request);
        }
    }

    @Override
    public void requestGallery(int request) {
        if (fragmentEventListener != null) {
            fragmentEventListener.requestGalleryPermission(request);
        }
    }

    @Override
    public void askGallery(int request, Uri tempUri) {
        if (fragmentEventListener != null) {
            fragmentEventListener.launchGalleryIntent(request, tempUri);
        }
    }

    @Override
    public void askCamera(int request, Uri tempUri) {
        if (fragmentEventListener != null) {
            fragmentEventListener.launchCameraIntent(request, tempUri);
        }
    }

    private void processAddClick() {
        if (getPresenter() != null) {
            getPresenter().onAddClick();
        }
    }

    public List<String> getPhotos() {
        if (getPresenter() != null) {
            return getPresenter().getPhotos();
        } else {
            return new ArrayList<>();
        }
    }
}
