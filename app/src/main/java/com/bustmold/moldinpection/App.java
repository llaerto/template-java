package com.bustmold.moldinpection;

import android.app.Application;

import com.bustmold.moldinpection.common.AppModule;
import com.bustmold.moldinpection.common.Constants;
import com.bustmold.moldinpection.di.AppComponent;
import com.bustmold.moldinpection.di.DaggerAppComponent;
import com.bustmold.moldinpection.network.NetModule;


public class App extends Application {
    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule(Constants.SERVER_URL))
                .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
