package com.bustmold.moldinpection.model;

import com.bustmold.moldinpection.common.Constants;
import com.bustmold.moldinpection.interfaces.model.PagerModelActions;
import com.bustmold.moldinpection.interfaces.presenter.PagerPresenterActions;

/**
 * Created by Mike Diachenko
 * on 08-Jun-17.
 * from Alienware
 */

public class PagerModel extends BaseModel<PagerPresenterActions.ModelActions> implements PagerModelActions {

    public PagerModel(PagerPresenterActions.ModelActions presenter) {
        super(presenter);
    }

    @Override
    public void send() {
//        AsyncTask<Void, Void, Boolean> asyncTask = new AsyncTask<Void, Void, Boolean>() {
//            private boolean validationError;
//
//            @Override
//            protected Boolean doInBackground(Void... args) {
//                try {
//                    validationError = false;
//                    return LogicFacade.getInstance().postApplicationData();
//                } catch (ValidationException e) {
//                    validationError = true;
//                    return false;
//                }
//            }
//
//            @Override
//            protected void onPreExecute() {
//                super.onPreExecute();
//                mProgressDialog = ProgressDialog.show(ApplicationActivity.this,
//                        getString(R.string.waiting),
//                        getString(R.string.send_message),
//                        true);
//            }
//
//            @Override
//            protected void onPostExecute(Boolean result) {
//                super.onPostExecute(result);
//                if (mProgressDialog != null) {
//                    mProgressDialog.dismiss();
//                }
//
//                int messageId;
//                if (result) {
//                    messageId = R.string.post_successful_message;
//                } else if (validationError) {
//                    messageId = R.string.validation_failed_message;
//                } else {
//                    messageId = R.string.post_failed_message;
//                }
//                Toast.makeText(ApplicationActivity.this, getString(messageId), Toast.LENGTH_LONG).show();
//            }
//        };
//        asyncTask.execute();
    }

    @Override
    public String getPhone() {
        return Constants.PHONE;
    }
}
