package com.bustmold.moldinpection.model;

import com.bustmold.moldinpection.common.AppError;
import com.bustmold.moldinpection.di.AppComponent;
import com.bustmold.moldinpection.interfaces.model.BaseModelActions;
import com.bustmold.moldinpection.interfaces.presenter.BasePresenterActions;

import io.reactivex.disposables.CompositeDisposable;

public class BaseModel<T extends BasePresenterActions.ModelActions>
        implements BaseModelActions {

    private T presenter;
    private CompositeDisposable compositeSubscription;

    protected BaseModel(T presenter) {
        this.presenter = presenter;
        compositeSubscription = new CompositeDisposable();
    }

    protected T getPresenter() {
        return presenter;
    }

    protected CompositeDisposable getCompositeSubscription() {
        return compositeSubscription;
    }

    /* Model operations: presenter -> model */

    @Override
    public void onDestroy() {
        presenter = null;
        compositeSubscription.clear();
    }

    @Override
    public AppComponent injector() {
        return presenter != null ? presenter.injector() : null;
    }

    /* Inner methods */

    protected void onServerLoadError(Throwable throwable) {
        if (getPresenter() != null) {
            AppError error = new AppError();
            error.setThrowable(throwable);
            getPresenter().onError(error);
        }
    }
}

