package com.bustmold.moldinpection.model;

import com.bustmold.moldinpection.interfaces.model.SplashModelActions;
import com.bustmold.moldinpection.interfaces.presenter.SplashPresenterActions;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Mike Diachenko
 * on 09-Jun-17.
 * from Alienware
 */

public class SplashModel extends BaseModel<SplashPresenterActions.ModelActions> implements SplashModelActions {
    private int progress = 0;
    private int max = 2;
    public SplashModel(SplashPresenterActions.ModelActions presenter) {
        super(presenter);
    }

    @Override
    public void startSplash() {
        tick();
    }

    private void tick() {
        getCompositeSubscription().add(Observable.just(1)
                .delay(1, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn(throwable -> null)
                .doOnComplete(this::processComplete)
                .subscribe());
    }

    private void processComplete() {
        progress++;
        if (progress == max) {
            if (getPresenter() != null) {
                getPresenter().onProgressUpdate(100);
                getPresenter().onSplashFinished();
            }
        } else {
            if (getPresenter() != null) {
                getPresenter().onProgressUpdate(100 * progress / max);
                tick();
            }
        }
    }
}

