package com.bustmold.moldinpection.model;

import com.bustmold.moldinpection.interfaces.model.InfoModelActions;
import com.bustmold.moldinpection.interfaces.presenter.InfoPresenterActions;

/**
 * Created by Mike Diachenko
 * on 08-Jun-17.
 * from Alienware
 */

public class InfoModel extends BaseModel<InfoPresenterActions.ModelActions> implements InfoModelActions {

    public InfoModel(InfoPresenterActions.ModelActions presenter) {
        super(presenter);
    }

    @Override
    public void preloadData() {

    }
}
