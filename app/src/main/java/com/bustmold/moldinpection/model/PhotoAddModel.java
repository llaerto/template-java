package com.bustmold.moldinpection.model;

import com.bustmold.moldinpection.interfaces.model.PhotoAddModelActions;
import com.bustmold.moldinpection.interfaces.presenter.PhotoAddPresenterActions;

/**
 * Created by Mike Diachenko
 * on 08-Jun-17.
 * from Alienware
 */

public class PhotoAddModel extends BaseModel<PhotoAddPresenterActions.ModelActions>
        implements PhotoAddModelActions {
    public PhotoAddModel(PhotoAddPresenterActions.ModelActions presenter) {
        super(presenter);
    }

    @Override
    public void loadPhotoGalery() {

    }

    @Override
    public void launchCamera() {

    }
}
