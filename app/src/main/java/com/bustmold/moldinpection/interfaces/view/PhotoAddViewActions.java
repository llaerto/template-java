package com.bustmold.moldinpection.interfaces.view;

import android.net.Uri;

import java.util.List;

/**
 * Created by Mike Diachenko
 * on 08-Jun-17.
 * from Alienware
 */

public interface PhotoAddViewActions extends BaseViewActions {
    void setupAdapter(List<String> list);
    void addPhoto();
    boolean isCameraPermitted();
    boolean isGalleryPermitted();
    void requestCamera(int request);
    void requestGallery(int request);
    void askGallery(int request, Uri tempUri);
    void askCamera(int request, Uri tempUri);
}
