package com.bustmold.moldinpection.interfaces.view;

/**
 * Created by Mike Diachenko
 * on 09-Jun-17.
 * from Alienware
 */

public interface SplashViewActions extends BaseViewActions {
    void launchApp();
    void updateProgress(int progress);
}
