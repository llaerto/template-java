package com.bustmold.moldinpection.interfaces.model;

/**
 * Created by Mike Diachenko
 * on 08-Jun-17.
 * from Alienware
 */

public interface PhotoAddModelActions extends BaseModelActions {
    void loadPhotoGalery();
    void launchCamera();
}
