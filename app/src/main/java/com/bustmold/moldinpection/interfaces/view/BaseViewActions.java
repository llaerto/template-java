package com.bustmold.moldinpection.interfaces.view;

import android.content.Context;
import android.support.annotation.Nullable;

import com.bustmold.moldinpection.common.AppError;
import com.bustmold.moldinpection.di.AppComponent;

public interface BaseViewActions {
    /* actions of model logic, for example - refresh ui */
    Context getContext();

    AppComponent injector();

    void showError(@Nullable AppError error);

    void showProgressBar();

    void showProgressView();

    void showEmptyView();

    void lockUi();

    boolean isUiLocked();

    void unlockUi();

    void hideKeyboard();
}
