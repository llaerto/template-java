package com.bustmold.moldinpection.interfaces.presenter;

import com.bustmold.moldinpection.entity.InfoEntity;

/**
 * Created by Mike Diachenko
 * on 08-Jun-17.
 * from Alienware
 */

public interface InfoPresenterActions extends BasePresenterActions {

    public interface ViewActions extends BasePresenterActions.ViewActions {

    }

    public interface ModelActions extends BasePresenterActions.ModelActions {
        void onDataPreloaded(InfoEntity entity);
    }
}
