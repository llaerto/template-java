package com.bustmold.moldinpection.interfaces.model;

/**
 * Created by Mike Diachenko
 * on 08-Jun-17.
 * from Alienware
 */

public interface InfoModelActions extends BaseModelActions {
    void preloadData();
}
