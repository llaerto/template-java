package com.bustmold.moldinpection.interfaces.model;

import com.bustmold.moldinpection.di.AppComponent;

public interface BaseModelActions {
    /* actions of model logic, for example - load data from server */
    void onDestroy();

    AppComponent injector();
}
