package com.bustmold.moldinpection.interfaces.model;

import com.bustmold.moldinpection.entity.InfoEntity;

import java.util.List;

/**
 * Created by Mike Diachenko
 * on 08-Jun-17.
 * from Alienware
 */

public interface PagerModelActions extends BaseModelActions {
    void send(InfoEntity entity, List<String> photos);
    String getPhone();
}
