package com.bustmold.moldinpection.interfaces.view;

import com.bustmold.moldinpection.entity.InfoEntity;

/**
 * Created by Mike Diachenko
 * on 08-Jun-17.
 * from Alienware
 */

public interface InfoViewActions extends BaseViewActions {
    void setDataPreloaded(InfoEntity entity);
}
