package com.bustmold.moldinpection.interfaces.presenter;

import java.util.List;

/**
 * Created by Mike Diachenko
 * on 08-Jun-17.
 * from Alienware
 */

public interface PhotoAddPresenterActions extends BasePresenterActions {

    public interface ViewActions extends BasePresenterActions.ViewActions {
        void onAddClick();
        List<String> getPhotos();
    }

    public interface ModelActions extends BasePresenterActions.ModelActions {
    }
}
