package com.bustmold.moldinpection.interfaces;

import android.content.Intent;

import com.bustmold.moldinpection.view.fragments.BaseFragment;


public interface BaseFragmentEventListener {
    void launchActivity(Class clzz, boolean isFinish);

    void launchIntent(Intent intent);

    void openFragment(BaseFragment fragment, boolean addToBackStack);

//    void showDialogFragment(BaseDialogFragment dialogFragment);
}
