package com.bustmold.moldinpection.interfaces.presenter;

/**
 * Created by Mike Diachenko
 * on 08-Jun-17.
 * from Alienware
 */

public interface SplashPresenterActions extends BasePresenterActions {

    public interface ViewActions extends BasePresenterActions.ViewActions {
    }

    public interface ModelActions extends BasePresenterActions.ModelActions {
        void onProgressUpdate(int progress);
        void onSplashFinished();
    }
}
