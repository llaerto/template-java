package com.bustmold.moldinpection.interfaces.view;

/**
 * Created by Mike Diachenko
 * on 08-Jun-17.
 * from Alienware
 */

public interface PagerViewActions extends BaseViewActions {
    void callUs(String phone);
}
