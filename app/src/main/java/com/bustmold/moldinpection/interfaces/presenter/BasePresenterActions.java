package com.bustmold.moldinpection.interfaces.presenter;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.bustmold.moldinpection.common.AppError;
import com.bustmold.moldinpection.di.AppComponent;
import com.bustmold.moldinpection.interfaces.view.BaseViewActions;

public interface BasePresenterActions {
    /* presenter actions - should communicate model with view through presenter */

    /* View -> Presenter actions
     * actions should communicate from view to presenter. Presenter should do something
     * and decide next step if it is necessary
     */
    interface ViewActions {
        void onViewCreated();

        void setView(BaseViewActions view);

        void onViewDestroyed();

        void onDestroy();

        void onStart();

        void onResume();

        void onPause();

        void onStop();

        void onActivityResult(int request, int result, Intent data);

        void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults);
    }

    /* Model -> Presenter
     * actions should communicate from model to presenter. Presenter should do something
     * and decide next step if it is necessary
     */
    interface ModelActions {
        void onError(@Nullable AppError appError);

        AppComponent injector();
    }
}
