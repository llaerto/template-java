package com.bustmold.moldinpection.interfaces.presenter;

import com.bustmold.moldinpection.entity.InfoEntity;

import java.util.List;

/**
 * Created by Mike Diachenko
 * on 08-Jun-17.
 * from Alienware
 */

public interface PagerPresenterActions extends BasePresenterActions {

    public interface ViewActions extends BasePresenterActions.ViewActions {
        void onCallClicked();
        void onSendClicked(InfoEntity infoEntity, List<String> photos);
    }

    public interface ModelActions extends BasePresenterActions.ModelActions {
        void onSuccessSend();
    }
}
