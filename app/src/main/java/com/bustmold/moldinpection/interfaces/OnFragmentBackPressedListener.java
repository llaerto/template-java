package com.bustmold.moldinpection.interfaces;

/**
 * Created by Vladislav Kavunenko on 01.05.17.
 * Vedidev, 2017
 */

public interface OnFragmentBackPressedListener {
    void onBackPressed();
}
