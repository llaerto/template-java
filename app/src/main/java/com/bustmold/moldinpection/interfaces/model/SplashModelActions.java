package com.bustmold.moldinpection.interfaces.model;

/**
 * Created by Mike Diachenko
 * on 09-Jun-17.
 * from Alienware
 */

public interface SplashModelActions extends BaseModelActions {
    void startSplash();
}
