package com.bustmold.moldinpection.common;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import java.util.HashMap;

public class StateMaintainer {

    private HashMap<String, Object> data;
    private static StateMaintainer instance;

    private StateMaintainer() {
        this.data = new HashMap<>();
        instance = this;
    }

    public boolean firstTimeIn(FragmentManager fragmentManager, String fragmentTag) {
        try {
            Fragment stateMaintainerFrag = fragmentManager.findFragmentByTag(fragmentTag);
            if (stateMaintainerFrag == null) {
                stateMaintainerFrag = new Fragment();
                fragmentManager.beginTransaction()
                        .add(stateMaintainerFrag, fragmentTag).commit();
                return true;
            }
            return false;
        } catch (NullPointerException e) {
            return false;
        }
    }

    public boolean firstTimeInForActivity(String activityTag) {
        return data == null || data.size() == 0 || data.containsKey(activityTag);
    }

    public static StateMaintainer getInstance() {
        if (instance == null) {
            instance = new StateMaintainer();
        }
        return instance;
    }

    public void delete(String key) {
        if (data != null && data.containsKey(key)) {
            data.remove(key);
        }
    }

    public void put(String key, Object obj) {
        data.put(key, obj);
    }

    @SuppressWarnings("unchecked")
    public <T> T get(String key) {
        return (T) data.get(key);
    }
}
