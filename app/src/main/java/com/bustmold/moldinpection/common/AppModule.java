package com.bustmold.moldinpection.common;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.bustmold.moldinpection.App;
import com.bustmold.moldinpection.bus.RxBus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


@Module
public class AppModule {
    private App app;

    public AppModule(App app) {
        this.app = app;
    }

    @Provides
    @Singleton
    App provideApp() {
        return app;
    }

    @Provides
    @Singleton
    RxBus provideRxBus() {
        return new RxBus();
    }

    @Provides
    @Singleton
    SharedPreferences providesSharedPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(app);
    }
}
