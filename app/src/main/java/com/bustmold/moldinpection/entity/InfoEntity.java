package com.bustmold.moldinpection.entity;

import com.bustmold.moldinpection.utils.ValidateUtils;

/**
 * Created by Mike Diachenko
 * on 08-Jun-17.
 * from Alienware
 */

public class InfoEntity {
    private String name;
    private String email;
    private String phone;
    private String country;
    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isFilled() {
        ValidateUtils validateUtils = new ValidateUtils();
        return !validateUtils.nullToString(name).isEmpty() &&
                !validateUtils.nullToString(email).isEmpty() &&
                !validateUtils.nullToString(phone).isEmpty() &&
                !validateUtils.nullToString(country).isEmpty() &&
                !validateUtils.nullToString(description).isEmpty();
    }
}
